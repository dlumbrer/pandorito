#!/usr/bin/env python

import json
import signal
import sys
import time
import os
import multiprocessing
from cryptopia_api import Api
from datetime import datetime

# Get these from (link here)
def get_secret(secret_file):
    """Grabs API key and secret from file and returns them"""

    with open(secret_file) as secrets:
        secrets_json = json.load(secrets)
        secrets.close()

    return str(secrets_json['key']), str(secrets_json['secret'])

def sigint_handler():
    """Handler for ctrl+c"""
    print '\n[!] CTRL+C pressed. Exiting...'
    sys.exit(0)

def update_market_prices(q):
    while True:
        MARKETS, ERROR = API.get_markets("BTC")
        if ERROR is not None:
            print ERROR
            return
        market_prices={'key':'value'}
        for i in range(len(MARKETS)):
            market_prices[MARKETS[i]['Label']] = MARKETS[i]['LastPrice']

        if q.qsize() > 0 :
            q.get()
            q.put(market_prices)
        else:
            q.put(market_prices)

        with open('datos.txt', 'ab') as file:
            file.write('\n')
            file.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            file.write('\n')
            file.write(json.dumps(market_prices))

        time.sleep(0.5)


EXIT_CYCLE = False
while not EXIT_CYCLE:

    # setup api
    #KEY, SECRET = get_secret("secrets.json")
    KEY = ''
    SECRET = ''
    API = Api(KEY, SECRET)

    # do before entering coin to save the API call during the pump


    queue = multiprocessing.Queue()
    p = multiprocessing.Process(target=update_market_prices, args=(queue,))
    p.start()

    signal.signal(signal.SIGINT, sigint_handler)

    PUMP_BALANCE = 0.005
    PUMP_BUY = 50.0
    PUMP_SELL = 100.0
    print '       You use         {}0000 BTC.\n'.format(PUMP_BALANCE)
    print '       Order buys automaticaly at top limit of +{}%.\n'.format(PUMP_BUY)
    print '\n      ->Press CTRL+C to exit at anytime.\n'

    PUMP_COIN = raw_input("       Coin Ticker Symbol: ")
    PUMP_COIN = PUMP_COIN.upper()

    MARKET_PRICES_LIST = queue.get()
    ASK_PRICE = MARKET_PRICES_LIST[PUMP_COIN+"/BTC"]
    ASK_BUY = ASK_PRICE + (float(PUMP_BUY/100) * ASK_PRICE)

    NUM_COINS = (PUMP_BALANCE - (PUMP_BALANCE*0.00201)) / ASK_BUY
    print '\n******* LastPrice of {} --> {:.8f}*******\n'.format(PUMP_COIN, ASK_PRICE)
    print '******* Buying {} for --> {:.8f}*******\n'.format(PUMP_COIN, ASK_BUY)

    TRADE, ERROR = API.submit_trade(PUMP_COIN + '/BTC', 'Buy', ASK_BUY, NUM_COINS)
    if ERROR is not None:
        print ERROR
        break

    AVAILABLE_PUMP_COINS = 0

    while AVAILABLE_PUMP_COINS == 0:
        time.sleep(0.1)
        COINS_OWNED, ERROR = API.get_balance(PUMP_COIN)
        if ERROR is not None:
            print ERROR
            AVAILABLE_PUMP_COINS = 0
        else:
           AVAILABLE_PUMP_COINS = COINS_OWNED['Available']


    BUY_PRICE = ASK_BUY * NUM_COINS

    print '\n[+] Buy order completed for  {:.8f} {} coins at {:.8f} BTC \
           total of {} BTC \n'.format(AVAILABLE_PUMP_COINS, PUMP_COIN, ASK_BUY, BUY_PRICE)

    ASK_SELL = ASK_PRICE + (PUMP_SELL/100 * ASK_PRICE)
    SELL_PRICE = ASK_SELL * AVAILABLE_PUMP_COINS
    print '\n\n\n****************************************************************\n'
    print '     Order sells automaticaly at +{}% == {:.8f}.\n'.format(PUMP_SELL,ASK_SELL)
    GREENLIGHTSELL = raw_input("DO YOU WANT SEND THE SELL ORDER? Press a Key to Continue: ")

    TRADE, ERROR = API.submit_trade(PUMP_COIN + '/BTC', 'Sell', ASK_SELL, AVAILABLE_PUMP_COINS)
    if ERROR is not None:
        print ERROR
        break

    SELL_PRICE = ASK_SELL * NUM_COINS

    print '\n[+] SELL order completed for {:.8f} {} coins at {:.8f} BTC \
           total of {} BTC'.format(NUM_COINS, PUMP_COIN, ASK_SELL, SELL_PRICE)

    if __name__ == "__main__":
        ANSWER = raw_input("\nWould you like to restart the Trade Bot? (y/n) ").format(PUMP_COIN)
        if ANSWER.lower().strip() in "n no".split():
            EXIT_CYCLE = True
            queue.close()
            queue.join_thread()
            p.join()
